from django.urls import path

from .views import (forum, ForumPostDetailView,
                    ForumPostCreateView, ForumPostUpdateView)

urlpatterns = [
    path('', forum, name='forum'),
    path('forumposts/<int:pk>/details/',
         ForumPostDetailView.as_view(),
         name='forumpost-details'),
    path('forumposts/add/',
         ForumPostCreateView.as_view(),
         name='forumpost-create'),
    path('forumposts/<int:pk>/edit/',
         ForumPostUpdateView.as_view(),
         name='forumpost-update'),
]

app_name = "forum"