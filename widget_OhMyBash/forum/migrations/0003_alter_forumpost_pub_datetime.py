# Generated by Django 4.1.7 on 2023-05-14 07:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0002_alter_reply_forumpost'),
    ]

    operations = [
        migrations.AlterField(
            model_name='forumpost',
            name='pub_datetime',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
