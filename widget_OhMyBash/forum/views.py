from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import ForumPost, Reply


def forum(request):
    posts = ForumPost.objects.all()
    return render(request, 'forum/forum.html', {'posts': posts})


class ForumPostDetailView(DetailView):
    model = ForumPost
    template_name = 'forum/forumpost-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['replys'] = Reply.objects.all()
        return context


class ForumPostCreateView(CreateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-add.html'


class ForumPostUpdateView(UpdateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-edit.html'