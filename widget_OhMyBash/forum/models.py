from django.db import models
from django.urls import reverse


class ForumPost(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    author = models.ForeignKey(
        'dashboard.WidgetUser',
        on_delete=models.CASCADE,
        related_name='forumpost_author'
    )
    pub_datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} by {} posted {}: {}'.format(
            self.title, 
            self.author, 
            self.pub_datetime,
            self.body
        )

    def get_absolute_url(self):
        return reverse(
            'forum:forumpost-details',
            kwargs={'pk': self.pk}
        )


class Reply(models.Model):
    body = models.TextField()
    author = models.ForeignKey(
        'dashboard.WidgetUser',
        on_delete=models.CASCADE,
        related_name='reply_author'
    )
    pub_datetime = models.DateTimeField()
    forumpost = models.ForeignKey(
        ForumPost,
        on_delete=models.CASCADE,
        related_name='forumpost_reply'
    )

    def __str__(self):
        return 'Reply by {} posted {}: {}'.format(
            self.author,
            self.pub_datetime,
            self.body,
        )