from django.db import models
from django.urls import reverse

class Location(models.Model):
    onsite = 'onsite'
    online = 'online'
    hybrid = 'hybrid'
    mode_choices = [
        (onsite, 'onsite'),
        (online, 'online'),
        (hybrid, 'hybrid'),
    ]
    mode = models.CharField(
        max_length=6, choices=mode_choices, default=onsite,
    )
    venue = models.CharField(max_length=100)

    def __str__(self):
        return '{}, {}'.format(self.mode, self.venue)
    



class Event(models.Model):
    target_datetime = models.DateTimeField()
    activity = models.CharField(max_length=255)
    estimated_hours = models.FloatField()
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    course = models.ForeignKey(
        'assignments.Course',
        on_delete=models.CASCADE,
        related_name="event_course"
    )

    def __str__(self):
        return '{}, {}'.format(self.activity, self.target_datetime)
    
    def get_absolute_url(self):
        return reverse('widget_calendar:event-details', kwargs={'pk': self.pk})