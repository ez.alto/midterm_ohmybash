from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView

from .models import Assignment


def Assignments(request):
    assignments = Assignment.objects.all()
    dict_assignments = {
        'assignments': assignments,
    }
    return render(request, 'assignments/assignments.html', dict_assignments)


class AssignmentDetailView(DetailView):
    model = Assignment
    template_name = 'assignments/assignment-details.html'


class AssignmentAddView(CreateView):
    model = Assignment
    fields = '__all__'
    template_name = 'assignments/assignment-add.html'


class AssignmentEditView(UpdateView):
    model = Assignment
    fields = '__all__'
    template_name = 'assignments/assignment-edit.html'
