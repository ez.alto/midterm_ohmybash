from django.contrib import admin
from .models import Event, Location


class EventAdmin(admin.ModelAdmin):
    model = Event
    search_fields = ('activity', 'target_datetime', 'course', )
    list_display = (
            'target_datetime', 'activity', 'estimated_hours',
            'location', 'course',
        )
    list_filter = ('target_datetime', 'course', )
    fieldsets = [
        ('Activity', {
            'fields': [
                    ('activity', 'course', ),
                    'target_datetime', 'location', 'estimated_hours'
                ]
        }),
    ]


class LocationAdmin(admin.ModelAdmin):
    model = Location
    search_fields = ('venue', 'mode', )
    list_display = ('mode', 'venue', )
    list_filter = ('mode', )


admin.site.register(Event, EventAdmin)
admin.site.register(Location, LocationAdmin)
