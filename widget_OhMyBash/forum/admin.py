from django.contrib import admin
from .models import ForumPost, Reply


class ReplyInline(admin.TabularInline):
    model = Reply


class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost

    list_display = ('title', 'body', 'author', 'pub_datetime',)
    search_fields = ('title', 'author',)
    list_filter = ('author', 'pub_datetime',)

    inlines = [ReplyInline,]

    fieldsets = [
        ('Forum Data', {
            'fields': [
                ('title', 'body'), 'author', 'pub_datetime'
            ]
        }),
    ]


class ReplyAdmin(admin.ModelAdmin):
    model = Reply

    list_display = ('body', 'author', 'pub_datetime',)
    search_fields = ('author',)
    list_filter = ('author', 'pub_datetime',)

    fieldsets = [
        ('Reply Data', {
            'fields': [
                ('forumpost', 'body'), 'author', 'pub_datetime'
            ]
        }),
    ]


admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)