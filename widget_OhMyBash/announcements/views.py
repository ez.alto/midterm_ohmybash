from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from .models import Announcement


def index(request):
    announcements = Announcement.objects.all()
    return render(
        request,
        'announcements/announcements.html',
        {'announcements': announcements},
    )


class AnnouncementsDetailView(DetailView):
    model = Announcement
    template_name = 'announcements/announcement-details.html'


class AnnouncementsCreateView(CreateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcements/announcement-add.html'
    

class AnnouncementsUpdateView(UpdateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcements/announcement-edit.html'
