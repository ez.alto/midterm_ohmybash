from django.urls import path
from .views import (calendar, EventDetailView, EventCreateView, EventUpdateView)

urlpatterns = [
    path('', calendar, name='calendar'),
    path('events/<int:pk>/details/', EventDetailView.as_view(), name='event-details'),
    path('events/add/', EventCreateView.as_view(), name='event-add'),
    path('events/<int:pk>/edit/', EventUpdateView.as_view(), name='event-edit'),

]
# This might be needed, depending on your Django version
app_name = "widget_calendar"