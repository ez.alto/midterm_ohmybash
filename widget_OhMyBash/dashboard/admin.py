from django.contrib import admin
from .models import Department, WidgetUser


class DepartmentAdmin(admin.ModelAdmin):
    model = Department

    list_display = ('dept_name', 'home_unit',)
    search_fields = ('dept_name', 'home_unit',)
    list_filter = ('dept_name', 'home_unit',)


class WidgetUserAdmin(admin.ModelAdmin):
    model = WidgetUser

    list_display = ('last_name', 'first_name', 'middle_name', 'department')
    search_fields = ('last_name', 'first_name', 'middle_name', 'department')
    list_filter = ('last_name', 'first_name', 'middle_name', 'department')


admin.site.register(Department, DepartmentAdmin)
admin.site.register(WidgetUser, WidgetUserAdmin)
