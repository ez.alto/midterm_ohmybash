BS Computer Science CSCI 40-L

Bolislis, Elaiza R.
Marcelo, Alliyah Ericka Therese E.
Mendoza, Juan Rafael D.
Salas, Jose Gabriel L.
Samson, Jerril Nheo A.

Final Project: Widget v2
Bolislis - Dashboard
Marcelo - Forum
Mendoza - Assignments
Salas - Calendar
Samson - Announcement Board

May 15, 2023

The group, OhMyBash, promises that this project was made by them.

References:

Dashboard:

https://www.computerhope.com/issues/ch001704.htm#:~:text=Adding%20the%20%22list%2Dstyle%3A,removes%20any%20bullet%20or%20number.
https://www.w3schools.com/howto/howto_css_list_without_bullets.asp
https://blog.hubspot.com/website/html-line-break#:~:text=To%20do%20a%20line%20break%20in%20HTML%2C%20use%20the%20%3Cbr,%3E%20and%20element.
https://stackoverflow.com/questions/70774525/how-to-add-custom-title-to-django-form-fields

Announcements: 

https://www.youtube.com/watch?v=cWq6jQGWmEg&ab_channel=VeryAcademy
https://www.codingninjas.com/codestudio/library/django-model-data-types-and-fields-list
https://automatetheboringstuff.com/chapter6/#:~:text=A%20multiline%20string%20in%20Python,lines%20inside%20a%20multiline%20string.
https://www.w3schools.com/tags/tag_br.asp

Forum:

https://docs.github.com/en/pull-requests/committing-changes-to-your-project/creating-and-editing-commits/changing-a-commit-message
https://betterprogramming.pub/how-to-use-pass-break-and-continue-in-python-6e0201fc032a
https://www.delftstack.com/howto/django/django-reset-database/ 
https://www.w3schools.com/django/ref_filters_slice.php

Calendar:

https://pythonguides.com/django-round-to-two-decimal-places/ 
https://www.geeksforgeeks.org/datetimefield-django-models/ 

(sgd) Elaiza R. Bolislis, 5/15/2023
(sgd) Alliyah Ericka Therese E. Marcelo, 5/15/2023
(sgd) Juan Rafael D. Mendoza, 5/15/2023
(sgd) Jose Gabriel L. Salas, 5/15/2023
(sgd) Jerril Nheo A. Samson, 5/15/2023
