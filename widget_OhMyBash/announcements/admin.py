from django.contrib import admin
from .models import Announcement, Reaction


class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement

    list_display = ('title', 'body', 'author', 'pub_datetime',)
    search_fields = ('title', 'body', 'author', 'pub_datetime',)
    list_filter = ('title', 'body', 'author', 'pub_datetime',)


class ReactionAdmin(admin.ModelAdmin):
    model = Reaction

    list_display = ('name', 'tally', 'announcement',)
    search_fields = ('name', 'tally', 'announcement',)
    list_filter = ('name', 'tally', 'announcement',)


admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)
