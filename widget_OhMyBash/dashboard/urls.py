from django.urls import path

from .views import (dashboard, WidgetUserDetailView,
                    WidgetUserCreateView, WidgetUserUpdateView)

urlpatterns = [
    path('dashboard/',
         dashboard,
         name='dashboard'),
    path('widgetusers/<int:pk>/details/',
         WidgetUserDetailView.as_view(),
         name='widgetuser-details'),
    path('widgetusers/add/',
         WidgetUserCreateView.as_view(),
         name='widgetuser-create'),
    path('widgetusers/<int:pk>/edit/',
         WidgetUserUpdateView.as_view(),
         name='widgetuser-update'),
]

app_name = "dashboard"
