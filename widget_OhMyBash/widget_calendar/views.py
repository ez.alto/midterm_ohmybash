from django.shortcuts import render, redirect
from django.views import View
from .models import Event
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView



def calendar(request):
    activities = Event.objects.all()
    return render(request, 'widget_calendar/calendar.html', {'activities':activities})

class EventDetailView(DetailView):
    model = Event
    template_name = "widget_calendar/event-details.html" 

class EventCreateView(CreateView):
    model = Event
    fields = '__all__'
    template_name = "widget_calendar/event-add.html"

class EventUpdateView(UpdateView):
    model = Event
    fields = '__all__'
    template_name = 'widget_calendar/event-edit.html'