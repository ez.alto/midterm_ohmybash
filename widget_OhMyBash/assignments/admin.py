from django.contrib import admin

from .models import Assignment, Course


class AssignmentAdmin(admin.ModelAdmin):
    model = Assignment

    list_display = ('name', 'description', 'course', 'perfect_score',)
    search_fields = ('name', 'description', 'course', 'perfect_score',)
    list_filter = ('name', 'description', 'course', 'perfect_score',)


class CourseAdmin(admin.ModelAdmin):
    model = Course

    list_display = ('code', 'title', 'section',)
    search_fields = ('code', 'title', 'section',)
    list_filter = ('code', 'title', 'section',)


admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(Course, CourseAdmin)
