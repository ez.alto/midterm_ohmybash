from django.db import models
from django.urls import reverse
from django.utils import timezone


class Announcement(models.Model):
    title = models.CharField(max_length=100, default="")
    body = models.TextField(default="")
    author = models.ForeignKey(
        'dashboard.WidgetUser',
        on_delete=models.CASCADE,
        related_name='announcements_author'
    )
    pub_datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{} by {} {} published {}, {}: {}'.format(
            self.title,
            self.author.first_name,
            self.author.last_name,
            self.pub_datetime.strftime("%m/%d/%Y"),
            self.pub_datetime.strftime("%H:%M %p"),
            self.body,
        )
    
    def format_date(self):
        return '{}' .format(self.pub_datetime.strftime("%m/%d/%Y"))
    
    def format_time(self):
        return '{}' .format(self.pub_datetime.strftime("%H:%M %p"))
    
    def getLike(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Like", announcement=self).tally)
        
        except:
            return '0'
    
    def getLove(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Love", announcement=self).tally)

        except:
            return '0'
    
    def getAngry(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Angry", announcement=self).tally)
        
        except:
            return '0'

    def get_absolute_url(self):
        return reverse(
            'announcements:announcement-details',
            kwargs={'pk': self.pk},
        )


class Reaction(models.Model):

    REACTION_LIKE = "LIKE"
    REACTION_LOVE = "LOVE"
    REACTION_ANGRY = "ANGRY"
    REACTION_CHOICES = [
       (REACTION_LIKE, "Like"),
       (REACTION_LOVE, "Love"),
       (REACTION_ANGRY, "Angry"),
    ]

    name = models.CharField(
        max_length=5,
        choices=REACTION_CHOICES,
        default=REACTION_LIKE,
        )
    tally = models.IntegerField(default=0)
    announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE)

    def __str__(self):
        return '{} reactions for {}' .format(self.name, self.announcement)
