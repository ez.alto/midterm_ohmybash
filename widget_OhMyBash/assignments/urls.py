from django.urls import path
from .views import Assignments, AssignmentDetailView, AssignmentAddView, AssignmentEditView

urlpatterns = [
    path('', Assignments, name="assignments"),
    path('<int:pk>/details/', AssignmentDetailView.as_view(), name='assignment-details'),
    path('add/', AssignmentAddView.as_view(), name='assignment-add'),
    path('<int:pk>/edit/', AssignmentEditView.as_view(), name='assignment-edit'),
]

app_name = 'assignments'
