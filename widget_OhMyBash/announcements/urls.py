from django.urls import path

from .views import (
    index,
    AnnouncementsDetailView,
    AnnouncementsCreateView,
    AnnouncementsUpdateView,
)

urlpatterns = [
    path('', index, name='index'),
    path('<int:pk>/details/',
         AnnouncementsDetailView.as_view(),
         name='announcement-details'),
    path('add/',
         AnnouncementsCreateView.as_view(),
         name='announcement-add'),
    path('<int:pk>/edit/',
         AnnouncementsUpdateView.as_view(),
         name='announcement-edit'),

]

app_name = "announcements"
